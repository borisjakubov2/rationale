package com.boris.rationale.ui;

import androidx.databinding.ObservableBoolean;
import androidx.hilt.lifecycle.ViewModelInject;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.boris.rationale.model.User;
import com.boris.rationale.repository.Repository;

import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.disposables.CompositeDisposable;
import io.reactivex.rxjava3.functions.Consumer;
import io.reactivex.rxjava3.schedulers.Schedulers;

public class ProfileViewModel extends ViewModel {

    private final Repository repository;
    public MutableLiveData<User> userMutableLiveData = new MutableLiveData<User>();
    private final CompositeDisposable disposables = new CompositeDisposable();
    public ObservableBoolean progressVisible = new ObservableBoolean(true);

    @ViewModelInject
    public ProfileViewModel(Repository repository) {
        this.repository = repository;
        getUserData();
    }

    public void getUserData(){
        progressVisible.set(true);
        disposables.add(repository.getUserData().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new Consumer<User>() {
            @Override
            public void accept(User user) throws Throwable {
                userMutableLiveData.setValue(user);
                progressVisible.set(false);
            }
        }, new Consumer<Throwable>() {
            @Override
            public void accept(Throwable throwable) throws Throwable {
                progressVisible.set(false);
            }
        }));
    }
}
