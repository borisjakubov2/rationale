package com.boris.rationale.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import androidx.activity.OnBackPressedCallback;
import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;

import com.boris.rationale.R;
import com.boris.rationale.databinding.FragmentFirstBinding;
import com.boris.rationale.model.User;


public class ProfileFragment extends Fragment {

    private FragmentFirstBinding binding;
    private ProfileViewModel profileViewModel;
    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState)
    {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_first, container,false);
        init();

        return binding.getRoot();
    }


    private void init() {
        profileViewModel = new ViewModelProvider(requireActivity()).get(ProfileViewModel.class);
        binding.setVm(profileViewModel);
    }
    private void observe() {
        profileViewModel.userMutableLiveData.observe(getViewLifecycleOwner(), new Observer<User>() {
            @Override
            public void onChanged(User user) {
                binding.setUser(user);
            }
        });
    }

    private void listeners() {
        binding.reposBtn.setOnClickListener(v->{
            NavHostFragment.findNavController(this).navigate(R.id.action_profileFragment_to_reposFragment);
        });


    }


    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        observe();
        listeners();
        super.onViewCreated(view, savedInstanceState);

    }
}