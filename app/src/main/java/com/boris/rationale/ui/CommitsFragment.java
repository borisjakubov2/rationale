package com.boris.rationale.ui;

import android.os.Bundle;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import com.boris.rationale.R;
import com.boris.rationale.adapters.CommitRecyclerAdapter;
import com.boris.rationale.databinding.FragmentCommitsBinding;
import com.boris.rationale.model.CommitResponse;

import java.util.ArrayList;


public class CommitsFragment extends Fragment {

    private FragmentCommitsBinding binding;
    private CommitsViewModel commitsViewModel;
    private CommitRecyclerAdapter recyclerAdapter;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("testtest", "onCreate: "+getArguments().getString("repoName"));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_commits,container,false);
        init();
        observe();
        return binding.getRoot();
    }


    private void init() {
        commitsViewModel = new ViewModelProvider(requireActivity()).get(CommitsViewModel.class);
        recyclerAdapter = new CommitRecyclerAdapter();
        recyclerAdapter.setHasStableIds(true);
        binding.commitsRecycler.setAdapter(recyclerAdapter);
        binding.commitsRecycler.setLayoutManager(new LinearLayoutManager(getContext()));
        binding.commitsRecycler.setHasFixedSize(true);
        binding.setVm(commitsViewModel);
        commitsViewModel.getRepoCommits(getArguments().getString("repoName"));
    }

    private void observe() {
        commitsViewModel.commitData.observe(getViewLifecycleOwner(), new Observer<ArrayList<CommitResponse>>() {
            @Override
            public void onChanged(ArrayList<CommitResponse> commits) {
                recyclerAdapter.setList(commits);
            }
        });
    }

}