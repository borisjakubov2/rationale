package com.boris.rationale.ui;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import androidx.activity.OnBackPressedCallback;
import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.LinearLayoutManager;


import com.boris.rationale.R;
import com.boris.rationale.adapters.ReposRecyclerAdapter;
import com.boris.rationale.databinding.FragmentSecondBinding;
import com.boris.rationale.model.Repo;

import java.util.ArrayList;

public class ReposFragment extends Fragment implements ReposRecyclerAdapter.OnRepoclick{

    private FragmentSecondBinding binding;
    private ReposViewModel reposViewModel;
    private ReposRecyclerAdapter reposRecyclerAdapter;
    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {

       binding = DataBindingUtil.inflate(inflater, R.layout.fragment_second,container,false);
       init();

       return binding.getRoot();
    }

    private void init() {
        reposViewModel = new ViewModelProvider(requireActivity()).get(ReposViewModel.class);
        binding.setVm(reposViewModel);
        reposRecyclerAdapter = new ReposRecyclerAdapter(this);
        reposRecyclerAdapter.setHasStableIds(true);
        binding.reposRecycler.setAdapter(reposRecyclerAdapter);
        binding.reposRecycler.setLayoutManager(new LinearLayoutManager(getContext()));
        binding.reposRecycler.setHasFixedSize(true);

    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        observe();

    }

    private void observe() {
        reposViewModel.arrayListMutableLiveData.observe(getViewLifecycleOwner(), new Observer<ArrayList<Repo>>() {
            @Override
            public void onChanged(ArrayList<Repo> repos) {
                reposRecyclerAdapter.setList(repos);
            }
        });
    }


    @Override
    public void onRepoClicked(String repoName) {
        Bundle bundle = new Bundle();
        bundle.putString("repoName",repoName);
        NavHostFragment.findNavController(this).navigate(R.id.action_reposFragment_to_commitsFragment,bundle);
    }
}