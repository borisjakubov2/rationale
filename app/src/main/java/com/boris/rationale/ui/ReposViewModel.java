package com.boris.rationale.ui;

import androidx.databinding.ObservableBoolean;
import androidx.hilt.lifecycle.ViewModelInject;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;


import com.boris.rationale.model.Repo;
import com.boris.rationale.repository.Repository;

import java.util.ArrayList;

import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.disposables.CompositeDisposable;
import io.reactivex.rxjava3.functions.Consumer;
import io.reactivex.rxjava3.schedulers.Schedulers;

public class ReposViewModel extends ViewModel {

    private final Repository repository;
    private final CompositeDisposable disposables = new CompositeDisposable();
    public MutableLiveData<ArrayList<Repo>> arrayListMutableLiveData = new MutableLiveData<>();
    public ObservableBoolean progressVisible = new ObservableBoolean(true);

    @ViewModelInject
    public ReposViewModel(Repository repository) {
        this.repository = repository;
        getRepos();
    }

    public void getRepos(){
        progressVisible.set(true);
        disposables.add(repository.getRepos().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new Consumer<ArrayList<Repo> >() {
            @Override
            public void accept(ArrayList<Repo> repos) throws Throwable {
                arrayListMutableLiveData.setValue(repos);
                progressVisible.set(false);
            }
        }, new Consumer<Throwable>() {
            @Override
            public void accept(Throwable throwable) throws Throwable {
                progressVisible.set(false);
            }
        }));
    }
}
