package com.boris.rationale.ui;

import androidx.databinding.ObservableBoolean;
import androidx.hilt.lifecycle.ViewModelInject;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.boris.rationale.model.CommitResponse;
import com.boris.rationale.repository.Repository;

import java.util.ArrayList;

import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.disposables.CompositeDisposable;
import io.reactivex.rxjava3.functions.Consumer;
import io.reactivex.rxjava3.schedulers.Schedulers;

public class CommitsViewModel extends ViewModel {

    private final Repository repository;
    public ObservableBoolean progressVissible = new ObservableBoolean(true);
    public MutableLiveData<ArrayList<CommitResponse>> commitData = new MutableLiveData<>();
    private final CompositeDisposable disposables = new CompositeDisposable();

    @ViewModelInject
    public CommitsViewModel(Repository repository) {
        this.repository = repository;
    }



    public void getRepoCommits(String reponame){
        progressVissible.set(true);
        disposables.add(repository.getCommits(reponame).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new Consumer<ArrayList<CommitResponse> >() {
            @Override
            public void accept(ArrayList<CommitResponse> repos) throws Throwable {
                commitData.setValue(repos);
                progressVissible.set(false);
            }
        }, new Consumer<Throwable>() {
            @Override
            public void accept(Throwable throwable) throws Throwable {
                progressVissible.set(false);
            }
        }));
    }
}
