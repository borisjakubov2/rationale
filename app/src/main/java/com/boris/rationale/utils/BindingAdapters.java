package com.boris.rationale.utils;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.databinding.BindingAdapter;

import com.bumptech.glide.Glide;

public class BindingAdapters {

    @BindingAdapter("imageUrl")
    public static void setProfileImage(AppCompatImageView image, String url){
        Glide.with(image.getContext()).load(url).into(image);
    }
}
