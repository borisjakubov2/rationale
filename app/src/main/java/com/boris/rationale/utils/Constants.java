package com.boris.rationale.utils;

public class Constants {
    public static final String BASE_URL = "https://api.github.com/";
    public static final String USER = "octocat";
    public static final String GET_PROFILE = BASE_URL + "users";
    public static final String GET_REPOS = GET_PROFILE+"/"+USER+"/repos";
    public static final String MY_TOKEN = "39ac901d1f2a30cb3eb5b28bc0d891639d1efef3";
}
