package com.boris.rationale.repository;






import com.boris.rationale.db.GitDao;
import com.boris.rationale.db.SharedPrefs;
import com.boris.rationale.model.CommitResponse;
import com.boris.rationale.model.Repo;
import com.boris.rationale.model.User;
import com.boris.rationale.network.ApiService;
import com.boris.rationale.utils.Constants;

import java.util.ArrayList;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.rxjava3.core.Observable;

@Singleton
public class Repository {

    ApiService apiService;
    GitDao gitDao;
    SharedPrefs sharedPrefs;


    @Inject
    public Repository(ApiService apiService, GitDao gitDao, SharedPrefs sharedPrefs) {
        this.apiService = apiService;
        this.gitDao = gitDao;
        this.sharedPrefs = sharedPrefs;

    }

    public Observable<User> getUserData(){
        return apiService.getUser(Constants.USER);
    }

    public Observable<ArrayList<Repo>> getRepos(){
        return apiService.getRepos();
    }

    public Observable<ArrayList<CommitResponse>> getCommits(String repo){
        return apiService.getRepoCommits(Constants.USER,repo);
    }
}
