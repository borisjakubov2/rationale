package com.boris.rationale.di;

import android.app.Application;

import androidx.room.Room;


import com.boris.rationale.db.AppDatabase;
import com.boris.rationale.db.GitDao;
import com.boris.rationale.db.SharedPrefs;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import dagger.hilt.InstallIn;
import dagger.hilt.android.components.ApplicationComponent;


@Module
@InstallIn(ApplicationComponent.class)
public class DatabaseModule {

    @Provides
    @Singleton
    AppDatabase provideAppDatabase(Application application){
        return Room.databaseBuilder(application, AppDatabase.class, "repo")
                .fallbackToDestructiveMigration()
                .allowMainThreadQueries()
                .build();
    }

    @Provides
    @Singleton
    GitDao provideTokenDao(AppDatabase appDatabase){
        return appDatabase.gitDao();
    }


    @Singleton
    @Provides
    public static SharedPrefs provideSharedPrefs(Application application){
        return SharedPrefs.getInstance(application);
    }


}
