package com.boris.rationale.db;

import android.content.Context;
import android.content.SharedPreferences;


public class SharedPrefs {

    private SharedPreferences prefs;
    private static SharedPrefs instance;

    private SharedPrefs(Context context) {
        prefs = context.getSharedPreferences("github", Context.MODE_PRIVATE);

    }

    public static SharedPrefs getInstance(Context context) {
        if (instance == null) {
            instance = new SharedPrefs(context);
        }
        return instance;
    }




}
