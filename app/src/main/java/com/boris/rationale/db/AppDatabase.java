package com.boris.rationale.db;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.boris.rationale.model.ExampleDB;


@Database(entities = {ExampleDB.class}, version = 4,exportSchema = false)

public abstract class AppDatabase extends RoomDatabase {

    public abstract GitDao gitDao();
}
