package com.boris.rationale.model;

import com.google.gson.annotations.SerializedName;

public class User {

    @SerializedName("login")
    public String name;

    @SerializedName("avatar_url")
    public String avatar;

    @SerializedName("company")
    public String companyName;

    @SerializedName("repos_url")
    public String repositories;
}
