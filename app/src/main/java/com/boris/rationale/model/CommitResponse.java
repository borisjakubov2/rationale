package com.boris.rationale.model;

public class CommitResponse {
    public String message;
    public Commit commit;
    public Author author;

    public static class Author {

        public String avatar_url;
    }
}
