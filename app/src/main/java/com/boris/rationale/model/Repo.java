package com.boris.rationale.model;

import com.google.gson.annotations.SerializedName;

public class Repo {



    public int id;

    @SerializedName("name")
    public String repoName;

    @SerializedName("open_issues_count")
    public int openIssues;
}
