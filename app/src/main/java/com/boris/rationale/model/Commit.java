package com.boris.rationale.model;

import com.google.gson.annotations.SerializedName;

public class Commit {

    public Author author;
    public String message;

    public static class Author {
        public String name;
        public String email;
        public String date;

    }
}
