package com.boris.rationale.model;


import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "repo")
public class ExampleDB {

    @PrimaryKey
    public int id;
    public String description;
}
