package com.boris.rationale.network;



import com.boris.rationale.model.CommitResponse;
import com.boris.rationale.model.Repo;
import com.boris.rationale.model.User;
import com.boris.rationale.utils.Constants;

import java.util.ArrayList;

import io.reactivex.rxjava3.core.Observable;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface ApiService {

    @GET(Constants.GET_PROFILE+ "/{name}")
    Observable<User> getUser(@Path("name") String userName);

    @GET(Constants.GET_REPOS)
    Observable<ArrayList<Repo>> getRepos();

    @GET(Constants.BASE_URL+"repos"+"/{name}"+"/{repo}"+"/commits")
    Observable<ArrayList<CommitResponse>> getRepoCommits(@Path("name") String name, @Path("repo")String repo);


}
