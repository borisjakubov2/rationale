package com.boris.rationale.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;


import com.boris.rationale.R;
import com.boris.rationale.databinding.SingleCommitItemBinding;
import com.boris.rationale.model.CommitResponse;

import java.util.ArrayList;

public class CommitRecyclerAdapter extends RecyclerView.Adapter<CommitRecyclerAdapter.CommitVh> {

    private ArrayList<CommitResponse> list = new ArrayList<>();

    public void setList(ArrayList<CommitResponse> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public CommitVh onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        SingleCommitItemBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.single_commit_item,parent,false);

        return new CommitVh(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull CommitVh holder, int position) {
        holder.binding.setCommit(list.get(position));
    }

    @Override
    public int getItemCount() {
        return list == null ? 0:list.size();
    }

    public class CommitVh extends RecyclerView.ViewHolder implements View.OnClickListener {
        SingleCommitItemBinding binding;

        public CommitVh(@NonNull SingleCommitItemBinding itemView) {
            super(itemView.getRoot());
            this.binding=itemView;
        }

        @Override
        public void onClick(View v) {

        }
    }
}
