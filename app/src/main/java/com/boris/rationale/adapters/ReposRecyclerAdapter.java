package com.boris.rationale.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;


import com.boris.rationale.R;
import com.boris.rationale.databinding.SingleRepoItemBinding;
import com.boris.rationale.model.Repo;

import java.util.ArrayList;

public class ReposRecyclerAdapter extends RecyclerView.Adapter<ReposRecyclerAdapter.ReposVH> {

    public ArrayList<Repo> list = new ArrayList<>();
    public OnRepoclick listener;

    public ReposRecyclerAdapter(OnRepoclick listener) {
        this.listener = listener;
    }

    public void setList(ArrayList<Repo> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ReposVH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        SingleRepoItemBinding repoItemBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.single_repo_item,parent,false);

        return new ReposVH(repoItemBinding,listener);
    }

    @Override
    public void onBindViewHolder(@NonNull ReposVH holder, int position) {
        holder.singleRepoItemBinding.setRepo(list.get(position));
    }

    @Override
    public int getItemCount() {
        return list == null ? 0:list.size();
    }

    public class ReposVH extends RecyclerView.ViewHolder implements View.OnClickListener {
        SingleRepoItemBinding singleRepoItemBinding;
        OnRepoclick onRepoclick;

        public ReposVH( SingleRepoItemBinding singleRepoItemBinding, OnRepoclick onRepoclick) {
            super(singleRepoItemBinding.getRoot());
            this.singleRepoItemBinding = singleRepoItemBinding;
            this.onRepoclick = onRepoclick;
            singleRepoItemBinding.getRoot().setOnClickListener(this);
        }


        @Override
        public void onClick(View v) {
            onRepoclick.onRepoClicked(list.get(getAdapterPosition()).repoName);
        }
    }

    public interface OnRepoclick {
        void onRepoClicked(String na
        );

    }
}
